﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace WebApi
{
    public class AuthOptions
    {
        public const string ISSUER = "TradeMarketWebAPI_JWT_AuthServer"; 
        public const string AUDIENCE = "TradeMarketWebAPI_JWT_Client"; 
        const string KEY = "YqQ_38Y9_2#4wW";   
        public const int LIFETIMEMINUTES = 20; 
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.Unicode.GetBytes(KEY));
        }
    }
}
