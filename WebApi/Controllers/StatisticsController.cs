﻿using Business.Interfaces;
using Business.Models;
using Business.Services;
using Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator, Manager")]
    public class StatisticsController : Controller
    {
        private readonly IStatisticService _statisticService;
        private readonly ILogger<StatisticsController> _logger;

        public StatisticsController(IStatisticService statisticService, ILogger<StatisticsController> logger)
        {
            _statisticService = statisticService;
            _logger = logger;
        }


        [HttpGet("popularProducts/")]
        public async Task<ActionResult<IEnumerable<ReceiptModel>>> GetMostPopularProducts([FromQuery]int productCount)
        {
            return Ok(await _statisticService.GetMostPopularProductsAsync(productCount));
        }

        [HttpGet("customer/{id}/{productCount}")]
        public async Task<ActionResult<IEnumerable<ReceiptModel>>> GetCustomerMostPopularProducts(int productCount, int id)
        {
            return Ok(await _statisticService.GetCustomersMostPopularProductsAsync(productCount, id));
        }
        
        [HttpGet("activity/{customerCount}")]
        public async Task<ActionResult<IEnumerable<ReceiptModel>>> GetMostValuableCustomersAsync(int customerCount, [FromQuery] DateTime startDate, DateTime endDate)
        {
            return Ok(await _statisticService.GetMostValuableCustomersAsync(customerCount, startDate, endDate));
        }

        [HttpGet("income/{categoryId}")]
        public async Task<ActionResult<IEnumerable<ReceiptModel>>> GetIncomeOfCategoryInPeriod(int categoryId, [FromQuery] DateTime startDate, DateTime endDate)
        {
            return Ok(await _statisticService.GetIncomeOfCategoryInPeriod(categoryId, startDate, endDate));
        }
    }
}
