﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Business.Validation.Filters;
using Data.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator, Manager")]
    public class CustomersController : Controller
    {
        //Inject customer service via constructor
        private readonly ICustomerService _customerService;
        private readonly ILogger<CustomersController> _logger;

        public CustomersController(ICustomerService customerService, ILogger<CustomersController> logger)
        {
            _customerService = customerService;
            _logger = logger;
        }

        // GET: api/customers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerModel>>> Get()
        {
            var customers = await _customerService.GetAllAsync();            
            return Ok(customers);
            
        }

        //GET: api/customers/1
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerModel>> GetById(int id)
        {
            var result = await _customerService.GetByIdAsync(id);

            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }
        
        //GET: api/customers/products/1
        [HttpGet("products/{id}")]
        public async Task<ActionResult<CustomerModel>> GetByProductId(int id)
        {
            return Ok(await _customerService.GetCustomersByProductIdAsync(id));
        }

        // POST: api/customers
        [HttpPost]
        [ModelValidationFilter]
        public async Task<ActionResult> Add([FromBody] CustomerModel value)
        {
            await _customerService.AddAsync(value);
            return Created("https://localhost:5001/api/customers", value);
            //try
            //{
            //    await _customerService.AddAsync(value);
            //}
            //catch(MarketException)
            //{
            //    return BadRequest();
            //}
            //return Created("https://localhost:5001/api/customers", value);
        }

        // PUT: api/customers/1
        [HttpPut("{id}")]
        [ModelValidationFilter]
        public async Task<ActionResult> Update(int id, [FromBody] CustomerModel value)
        {
            if (await _customerService.GetByIdAsync(id) == null)
                return NotFound();

            await _customerService.UpdateAsync(value);
            return Ok();
            //try
            //{
            //    await _customerService.UpdateAsync(value);

            //}
            //catch(MarketException)
            //{
            //    return BadRequest();
            //}
            //return Ok();
        }

        // DELETE: api/customers/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var toBeDeleted = await _customerService.GetByIdAsync(id);
            if (toBeDeleted == null)
                return NotFound();

            await _customerService.DeleteAsync(id);
            return Ok();
        }
    }
}
