﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : Controller
    {
        [Authorize]
        [HttpGet]
        [Route("getlogin/")]
        public IActionResult GetLogin()
        {
            return Ok($"Your login: {User.Identity.Name}");
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        [Route("getrole/")]
        public IActionResult GetRole()
        {
            return Ok("Your role is: Administrator.");
        }
    }
}
