﻿using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReceiptsController : Controller
    {
        private readonly IReceiptService _receiptService;
        private readonly ILogger<ReceiptsController> _logger;

        public ReceiptsController(IReceiptService receiptService, ILogger<ReceiptsController> logger)
        {
            _receiptService = receiptService;
            _logger = logger;
        }

        //GET: api/receipts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReceiptModel>>> Get()
        {    
            var receipts = await _receiptService.GetAllAsync();
            //_logger.LogInformation();
            return Ok(receipts);
        }

        //GET: api/receipts/period
        [HttpGet("period/")]
        public async Task<ActionResult<IEnumerable<ReceiptModel>>> Get([FromQuery] DateTime? startDate, DateTime? endDate)
        {
            var allReceipts = await _receiptService.GetAllAsync();
            var filteredByDateReceipts = allReceipts
                .Where(r => r.OperationDate >= (startDate ?? new DateTime(1990, 1, 1)) && 
                            r.OperationDate <= (endDate ?? DateTime.Now));
            //_logger.LogInformation();
            return Ok(filteredByDateReceipts);
        }

        //GET: api/receipts/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<ReceiptModel>> GetById(int id)
        {
            var result = await _receiptService.GetByIdAsync(id);

            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

        //GET: api/receipts/{id}/details
        [HttpGet("{id}/details")]
        public async Task<ActionResult<ReceiptModel>> GetReceiptDetailsById(int id)
        {
            var result = await _receiptService.GetReceiptDetailsAsync(id);

            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

        //GET: api/receipts/{id}/sum
        [HttpGet("{id}/sum")]
        public async Task<ActionResult<decimal>> GetReceiptSumById(int id)
        {
            var result = await _receiptService.ToPayAsync(id);
            return Ok(result);
        }

        // POST: api/receipts
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] ReceiptModel value)
        {
            try
            {
                await _receiptService.AddAsync(value);
            }
            catch (MarketException)
            {
                return BadRequest();
            }
            return Created("https://localhost:5001/api/receipts", value);
        }

        //PUT: api/receipts/{id}
        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] ReceiptModel value)
        {
            if (await _receiptService.GetByIdAsync(id) == null)
                return NotFound();

            try
            {
                await _receiptService.UpdateAsync(value);

            }
            catch (MarketException)
            {
                return BadRequest();
            }
            return Ok();
        }

        //PUT: api/receipts/{id}/products/add/{productId}/{quantity}
        [HttpPut("{id}/products/add/{productId}/{quantity}")]
        public async Task<ActionResult> AddToReceipt(int id, int productId, int quantity, [FromQuery] ReceiptDetailModel receiptDetailModel)
        {
            if(!_receiptService.GetAllAsync().Result.Select(r => r.Id).Contains(id))
                return BadRequest();
            
            await _receiptService.AddProductAsync(productId, id, quantity);
            return Ok();
        }

        //PUT: api/receipts/{id}/products/remove/{productId}/{quantity}
        [HttpPut("{id}/products/remove/{productId}/{quantity}")]
        public async Task<ActionResult> RemoveFromReceipt(int id, int productId, int quantity)
        {
            if (!_receiptService.GetAllAsync().Result.Select(r => r.Id).Contains(id))
                return BadRequest();

            await _receiptService.RemoveProductAsync(productId, id, quantity);
            return Ok();
        }

        //PUT: api/receipts/{id}/checkout
        [HttpPut("{id}/checkout")]
        public async Task<ActionResult> CheckOut(int id)
        {
            var targetReceipt = await _receiptService.GetByIdAsync(id);

            if (targetReceipt == null)
            {
                return NotFound();
            }
            await _receiptService.CheckOutAsync(id);
            
            return Ok();
        }

        // DELETE: api/receipts/{id}
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var toBeDeleted = await _receiptService.GetByIdAsync(id);
            if (toBeDeleted == null)
                return NotFound();

            await _receiptService.DeleteAsync(id);
            return Ok();
        }

    }
}
