﻿using Business.Interfaces;
using Business.Models;
using Business.Services;
using Business.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator, Manager")]
    public class ProductsController : Controller
    {
        private readonly IProductService _productService;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(IProductService productService, ILogger<ProductsController> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        //GET: api/products
        //GET: api/products?categoryId=1&minPrice=20&maxPrice=50
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductModel>>> Get([FromQuery] int? categoryId, int? minPrice, int? maxPrice)
        {
            if(categoryId != null || minPrice != null || maxPrice != null)
            {
                FilterSearchModel newFilter = new FilterSearchModel()
                {
                    CategoryId = categoryId,
                    MinPrice = minPrice,
                    MaxPrice = maxPrice
                };
                return Ok(await _productService.GetByFilterAsync(newFilter));
            }

            var customers = await _productService.GetAllAsync();
            //_logger.LogInformation();
            return Ok(customers);            
        }

        //GET: api/products/1
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductModel>> GetById(int id)
        {
            var result = await _productService.GetByIdAsync(id);

            if (result == null)
                return NotFound();
            else
                return Ok(result);
        }

        // POST: api/products
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] ProductModel value)
        {
            var targetCategory = await _productService.GetAllProductCategoriesAsync().ContinueWith(x => x.Result.SingleOrDefault(c => c.Id == value.ProductCategoryId));

            if(targetCategory == null)
            {
                return BadRequest("No such category in the system!");
            }

            value.CategoryName ??= targetCategory.CategoryName;

            try
            {
                await _productService.AddAsync(value);
            }
            catch (MarketException)
            {
                return BadRequest();
            }
            return Created("https://localhost:5001/api/products", value);
        }

        // PUT: api/products/1
        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] ProductModel value)
        {
            if (await _productService.GetByIdAsync(id) == null)
                return NotFound();

            try
            {
                await _productService.UpdateAsync(value);

            }
            catch (MarketException)
            {
                return BadRequest();
            }
            return Ok();
        }

        // DELETE: api/products/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var toBeDeleted = await _productService.GetByIdAsync(id);
            if (toBeDeleted == null)
                return NotFound();

            await _productService.DeleteAsync(id);
            return Ok();
        }

        //DELETE: api/products/categories/1
        [HttpDelete("categories/{categoryId}")]
        public async Task<ActionResult> DeleteCategory(int categoryId)
        {
            var toBeDeleted = await _productService.GetAllProductCategoriesAsync()
                .ContinueWith(x => x.Result.SingleOrDefault(r => r.Id == categoryId));
            if (toBeDeleted == null)
                return NotFound();

            await _productService.RemoveCategoryAsync(categoryId);
            return Ok();
        }

        // Get: api/products/categories
        [HttpGet("categories/")]
        public async Task<ActionResult<IEnumerable<ProductCategoryModel>>> GetAllCategories()
        {
            var allCategories = await _productService.GetAllProductCategoriesAsync();
            //_logger.LogInformation();
            return Ok(allCategories);
        }

        // POST: api/products/categories
        [HttpPost("categories/")]
        public async Task<ActionResult> Add([FromBody] ProductCategoryModel value)
        {
            try
            {
                await _productService.AddCategoryAsync(value);
            }
            catch (MarketException)
            {
                return BadRequest();
            }
            return Created("https://localhost:5001/api/products/categories", value);
        }

        //PUT: /api/products/categories/{id}
        [HttpPut("categories/{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] ProductCategoryModel value)
        {
            var targetCategory = await _productService.GetAllProductCategoriesAsync().ContinueWith(x => x.Result.SingleOrDefault(c => c.Id == value.Id));

            if (targetCategory == null)
            {
                return NotFound(); 
            }
           
            try
            {
                await _productService.UpdateCategoryAsync(value);

            }
            catch (MarketException)
            {
                return BadRequest();
            }
            return Ok();
        }

    }
}
