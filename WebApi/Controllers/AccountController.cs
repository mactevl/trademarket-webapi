﻿using Business.Interfaces;
using Business.Models;
using Business.Helpers;
using Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;
using Microsoft.Extensions.Configuration;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly ICustomerService _customerService;
        private readonly IMailService _mailService;
        private readonly IConfiguration _config;
        private readonly ILogger<AccountController> _logger;

        public AccountController(IAccountService accountService, 
                                 ICustomerService customerService,
                                 IMailService mailService,
                                 IConfiguration config,
                                 ILogger<AccountController> logger)
        {
            _accountService = accountService;
            _customerService = customerService;
            _mailService = mailService;
            _config = config;
            _logger = logger;
        }

        //Post: api/account/signup
        [AllowAnonymous]
        [HttpPost("signup/")]
        public async Task<IActionResult> RegisterNewUserAsync([FromBody] CustomerRegistrationModel signUpModel)
        {
            User user = await _accountService.GetUserByLoginAsync(signUpModel.Logon);

            if (user != null)
                return BadRequest("The email is already registered!");

            //adding new customer
            CustomerModel newCustomer = _accountService.GetCustomerModel(signUpModel);
            await _customerService.AddAsync(newCustomer);

            //checking if new customer added
            var allCustomers = await _customerService.GetAllAsync();
            CustomerModel addedCustomer = allCustomers
                .SingleOrDefault(cm => cm.Name == signUpModel.Name &&
                                       cm.Surname == signUpModel.Surname &&
                                       cm.BirthDate == signUpModel.BirthDate);

            //prep and adding new user
            signUpModel.CustomerId = addedCustomer.Id;
            await _accountService.RegisterNewUser(signUpModel);

            //checking if new user had been added
            User newUser = await _accountService.GetUserWithDetailsByLoginAsync(signUpModel.Logon);

            if (newUser == null)
                return BadRequest("New User had not been registered");

            //sending the confirmation email 
            string link = $"<a href=\"{_config.GetSection("WebAPIurl").Value}/api/account/verify?emailtoken={newUser.VerificationToken}\">Your confirmation link.</a>";

            MailRequest confirmationEmail = new MailRequest()
            {
                ToEmail = $"{newUser.Logon}",
                Subject = "TradeMarket. Email confirmation letter.",
                Body = $"&emsp;<b>{newUser.Customer.Person.Name} {newUser.Customer.Person.Surname}</b>, we are glad to welcome you in our store.<br>Please confirm your email by pressing on the link below - in case we'll need to contact you.<br>Again, we are pleased to see you in our circle and have a good shopping!<br>{link}"
            };
            await _mailService.SendEmailAsync(confirmationEmail);

            return Created("https://localhost:5001/api/account/signup", signUpModel);                
        }

        //Post: api/account/verify
        [AllowAnonymous]
        [HttpGet("verify")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> VerifyUserAsync([FromQuery] string emailtoken)
        {
            if (!await _accountService.IsVarified(emailtoken))
            {
                return BadRequest(new { errorText = "Invalid operation!" });
            }

            return Ok($"Email: {User.Identity.Name} verified successfully! You may proceed with logging in now.");
        }

        //Post: api/account/login
        [HttpPost("login/")]
        [AllowAnonymous]
        public async Task<IActionResult> LoginAsync([FromBody] UserLoginModel userModel)
        {
            User isInUsers = await _accountService.GetUserByLoginAsync(userModel.Logon);

            // if there is no such a user
            if (isInUsers == null ||
                !PasswordHasher.VerifySaltedPassword(userModel.Password, isInUsers.Password, isInUsers.PasswordSalt))
            {
                return BadRequest(new { errorText = "Invalid username or password!" });
            }

            // if user's email is still not varified
            if (isInUsers.VerifiedAt == null)
                return BadRequest(new { errorText = "You have to verify your email first!" });

            //if user is authenticated and verified
            var identity = await GetIdentity(userModel.Logon);       

            var now = DateTime.UtcNow;
            // JWT-creation
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIMEMINUTES)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };

            return Ok(Json(response));
        }

        //Post: api/account/forgot-password
        [HttpPost("forgot-password/")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPasswordAsync([FromQuery] string email)
        {
            User isInUsers = await _accountService.GetUserWithDetailsByLoginAsync(email);

            // if there is no such a user
            if (isInUsers == null)
            {
                return BadRequest(new { errorText = "User not found!" });
            }

            // if user's email is still not varified
            if (isInUsers.VerifiedAt == null)
                return BadRequest(new { errorText = "You have to verify your email first!" });

            //if user is authenticated and verified           
            await _accountService.GetPasswordResetTokenEmailAsync(email);

            //sending email
            MailRequest forgotPasswordEmail = new MailRequest()
            {
                ToEmail = $"{isInUsers.Logon}",
                Subject = "TradeMarket. Password reset procedure started.",
                Body = $"&emsp;<b>{isInUsers.Customer.Person.Name} {isInUsers.Customer.Person.Surname}</b>, you are about to set a new password for your account.<br>Please proceed using the code below to set your new password.<br>Confirmation code is: {isInUsers.PasswordResetToken}<br>It expires: {DateTime.UtcNow.AddDays(1).ToLocalTime()}"
            };
            await _mailService.SendEmailAsync(forgotPasswordEmail);

            return Ok("You may now reset your password. Check your email to proceed.");
        }

        //Post: api/account/reset-password
        [HttpPost("reset-password/")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPasswordAsync([FromBody] ResetPasswordRequest resetPasswordRequest)
        {
            // if there is no such a user
            if (!await _accountService.IsInUsers(resetPasswordRequest.Token))
            {
                return BadRequest(new { errorText = "Invalid token received!" });
            }

            //getting modified user
            User isInUsers = await _accountService.GetUserWithDetailsByTokenAsync(resetPasswordRequest.Token);

            //if user is authenticated and verified           
            if (!await _accountService.ResetPasswordAsync(resetPasswordRequest))
                return BadRequest("Your link is expired! You'll need to get a new one.");           

            //sending email
            MailRequest passwordResetEmail = new MailRequest()
            {
                ToEmail = $"{isInUsers.Logon}",
                Subject = "TradeMarket. New account password set successfully.",
                Body = $"&emsp;<b>{isInUsers.Customer.Person.Name} {isInUsers.Customer.Person.Surname}</b>, you've just reset your password.<br>Please log in using your new credentials."
            };
            await _mailService.SendEmailAsync(passwordResetEmail);

            return Ok("Password had been changed successfully! You may now proceed and login using your new credentials."); 
                
        }

        
        #region Auxiliary methods
        private async Task<ClaimsIdentity> GetIdentity(string username)
        {            
            User user = await _accountService.GetUserByLoginAsync(username);

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Logon),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.RoleName)
            };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;            
        }

        #endregion Auxiliary methods

    }
}
