﻿using Business.Models;
using Data.Data;
using Data.Entities;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TradeMarket.Tests.IntegrationTests
{
    public class AccountIntegrationTest
    {
        private CustomWebApplicationFactory _factory;
        private HttpClient _client;
        private const string RequestUri = "api/account/signup/";

        [SetUp]
        public void Init()
        {
            _factory = new CustomWebApplicationFactory();
            _client = _factory.CreateClient();
        }

        [Test]
        public async Task AccountController_RegisterNewUserAsync_ReturnsCreated()
        {
            //arrange
            var newCustomerRegistrationModel = new CustomerRegistrationModel
            {
                Name = "Desmond",
                Surname = "Morris",
                BirthDate = new DateTime(1967, 4, 12),
                Logon = "moris_john@aol.com",
                Password = "qQ123!!!"
            };

            // act
            var context = new StringContent(JsonConvert.SerializeObject(newCustomerRegistrationModel), Encoding.UTF8, "application/json");
            var httpResponse = await _client.PostAsync(RequestUri, context);

            // assert
            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var actual = JsonConvert.DeserializeObject<CustomerRegistrationModel>(stringResponse);
            newCustomerRegistrationModel.CustomerId = actual.CustomerId;

            actual.Should().BeEquivalentTo(newCustomerRegistrationModel);
        }       

        #region  helpers

        private async Task CheckExceptionWhileAddNewModel(CustomerModel customer)
        {
            var context = new StringContent(JsonConvert.SerializeObject(customer), Encoding.UTF8, "application/json");
            var httpResponse = await _client.PostAsync(RequestUri, context);

            httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        private async Task CheckExceptionWhileUpdateModel(CustomerModel customer)
        {
            var context = new StringContent(JsonConvert.SerializeObject(customer), Encoding.UTF8, "application/json");
            var httpResponse = await _client.PutAsync(RequestUri + customer.Id, context);

            httpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        private async Task CheckCustomersInfoIntoDb(CustomerModel customer, int customerId, int expectedLength)
        {
            using (var test = _factory.Services.CreateScope())
            {
                var context = test.ServiceProvider.GetService<TradeMarketDbContext>();
                context.Customers.Should().HaveCount(expectedLength);

                var dbCustomer = await context.Customers.FindAsync(customerId);
                dbCustomer.Should().NotBeNull();
                dbCustomer.DiscountValue.Should().Be(dbCustomer.DiscountValue);

                var dbPerson = await context.Persons.FindAsync(customerId);
                dbPerson.Should().NotBeNull().And.BeEquivalentTo(customer, options => options
                    .Including(x => x.Name)
                    .Including(x => x.Surname)
                    .Including(x => x.BirthDate)
                );
            }
        }
        #endregion

        [TearDown]
        public void TearDown()
        {
            _factory.Dispose();
            _client.Dispose();
        }

    }
}
