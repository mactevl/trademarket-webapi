﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entities
{
    public class Product : BaseEntity
    {
        public int? ProductCategoryId { get; set; }

        [Required]
        [MinLength(1),MaxLength(150)]
        public string ProductName { get; set; }

        [Required]
        public decimal Price { get; set; }

        public ProductCategory Category { get; set; }
        public ICollection<ReceiptDetail> ReceiptDetails { get; set; }
    }
}
