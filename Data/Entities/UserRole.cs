﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Entities
{
    public class UserRole : BaseEntity
    {
        [Required]
        [MinLength(1), MaxLength(150)]
        public string RoleName { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
