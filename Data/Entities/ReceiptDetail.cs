﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Entities
{
    public class ReceiptDetail : BaseEntity
    {
        public int ReceiptId { get; set; }
        public int ProductId { get; set; }
        public decimal DiscountUnitPrice { get; set; }
        public decimal UnitPrice { get; set; }

        [Range(0, 999)]
        public int Quantity { get; set; }

        public Product  Product { get; set; }
        public Receipt  Receipt { get; set; }
    }
}
