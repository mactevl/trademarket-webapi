﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Diagnostics.CodeAnalysis;

namespace Data.Entities
{
    public  class User : BaseEntity
    {
        public int? CustomerId { get; set; }

        [Required]
        public int? UserRoleId { get; set; }

        [Required]
        public string Logon { get; set; }

        [Required]
        public byte[] Password { get; set; }

        [Required]
        public byte[] PasswordSalt { get; set; }

        [Required]
        public string? VerificationToken { get; set; }
        public DateTime? VerifiedAt { get; set; }
        public string? PasswordResetToken { get; set; }
        public DateTime? ResetTokenExpires { get; set; }

        [AllowNull]
        public Customer? Customer { get; set; }
        public UserRole Role { get; set; }

        
    }
}
