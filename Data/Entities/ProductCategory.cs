﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Entities
{
    public class ProductCategory : BaseEntity
    {
        [Required]
        [MinLength(1), MaxLength(150)]
        public string CategoryName { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
