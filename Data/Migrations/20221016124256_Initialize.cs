﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class Initialize : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 30, nullable: false),
                    Surname = table.Column<string>(maxLength: 50, nullable: false),
                    BirthDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PersonId = table.Column<int>(nullable: false),
                    DiscountValue = table.Column<int>(nullable: false, defaultValue: 0)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductCategoryId = table.Column<int>(nullable: false),
                    ProductName = table.Column<string>(maxLength: 40, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(7,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_ProductCategories_ProductCategoryId",
                        column: x => x.ProductCategoryId,
                        principalTable: "ProductCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Receipts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerId = table.Column<int>(nullable: false),
                    OperationDate = table.Column<DateTime>(nullable: false),
                    IsCheckedOut = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Receipts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Receipts_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerId = table.Column<int>(nullable: true),
                    UserRoleId = table.Column<int>(nullable: false),
                    Logon = table.Column<string>(maxLength: 40, nullable: false),
                    Password = table.Column<byte[]>(nullable: false),
                    PasswordSalt = table.Column<byte[]>(nullable: false),
                    VerificationToken = table.Column<string>(nullable: false),
                    VerifiedAt = table.Column<DateTime>(nullable: true),
                    PasswordResetToken = table.Column<string>(nullable: true),
                    ResetTokenExpires = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_UserRoles_UserRoleId",
                        column: x => x.UserRoleId,
                        principalTable: "UserRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReceiptsDetails",
                columns: table => new
                {
                    ReceiptId = table.Column<int>(nullable: false),
                    ProductId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DiscountUnitPrice = table.Column<decimal>(type: "decimal(6,2)", nullable: false, defaultValue: 0m),
                    UnitPrice = table.Column<decimal>(type: "decimal(6,2)", nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceiptsDetails", x => new { x.ReceiptId, x.ProductId });
                    table.ForeignKey(
                        name: "FK_ReceiptsDetails_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ReceiptsDetails_Receipts_ReceiptId",
                        column: x => x.ReceiptId,
                        principalTable: "Receipts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Persons",
                columns: new[] { "Id", "BirthDate", "Name", "Surname" },
                values: new object[,]
                {
                    { 1, new DateTime(1980, 5, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Name1", "Surname1" },
                    { 2, new DateTime(1984, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Name2", "Surname2" }
                });

            migrationBuilder.InsertData(
                table: "ProductCategories",
                columns: new[] { "Id", "CategoryName" },
                values: new object[,]
                {
                    { 1, "Category1" },
                    { 2, "Category2" }
                });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "Id", "RoleName" },
                values: new object[,]
                {
                    { 1, "Administrator" },
                    { 2, "Manager" },
                    { 3, "Client" }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "DiscountValue", "PersonId" },
                values: new object[,]
                {
                    { 1, 20, 1 },
                    { 2, 10, 2 }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Price", "ProductCategoryId", "ProductName" },
                values: new object[,]
                {
                    { 1, 20m, 1, "Name1" },
                    { 2, 50m, 2, "Name2" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CustomerId", "Logon", "Password", "PasswordResetToken", "PasswordSalt", "ResetTokenExpires", "UserRoleId", "VerificationToken", "VerifiedAt" },
                values: new object[,]
                {
                    { 1, null, "vplatus@gmail.com", new byte[] { 214, 194, 181, 136, 232, 49, 69, 229, 152, 112, 171, 33, 253, 75, 46, 53, 164, 224, 98, 36, 113, 110, 135, 96, 82, 64, 60, 112, 139, 91, 44, 174, 27, 209, 172, 235, 246, 142, 165, 155, 122, 7, 59, 69, 71, 175, 190, 139, 16, 243, 220, 57, 183, 93, 27, 55, 18, 5, 55, 151, 226, 125, 114, 22 }, null, new byte[] { 115, 9, 44, 113, 172, 90, 88, 30, 24, 54, 188, 172, 107, 167, 128, 67, 184, 191, 199, 43, 16, 195, 1, 47, 80, 182, 71, 34, 94, 86, 223, 230, 40, 189, 28, 15, 196, 103, 50, 63, 173, 68, 71, 240, 53, 199, 204, 174, 82, 49, 202, 151, 36, 94, 6, 47, 98, 119, 125, 187, 152, 227, 39, 253, 100, 104, 68, 100, 86, 188, 40, 186, 227, 102, 152, 77, 195, 67, 143, 231, 84, 229, 242, 175, 104, 26, 225, 77, 141, 68, 205, 104, 194, 137, 30, 27, 224, 45, 2, 117, 100, 9, 86, 17, 102, 51, 254, 217, 66, 88, 51, 70, 135, 45, 95, 110, 35, 243, 227, 66, 132, 75, 111, 22, 17, 211, 199, 17 }, null, 1, "reqnybSxAkivr/SZ+nZtWw==", new DateTime(2022, 10, 16, 15, 42, 55, 452, DateTimeKind.Local).AddTicks(6132) },
                    { 2, null, "jack_sparrow@gmail.com", new byte[] { 187, 129, 229, 10, 233, 10, 16, 94, 185, 135, 251, 105, 209, 255, 30, 245, 156, 188, 171, 208, 177, 237, 35, 29, 203, 39, 79, 54, 30, 250, 121, 235, 9, 224, 226, 76, 199, 118, 210, 199, 175, 156, 209, 208, 88, 55, 105, 80, 82, 8, 8, 60, 36, 28, 149, 204, 85, 240, 23, 9, 175, 106, 79, 167 }, null, new byte[] { 6, 232, 148, 165, 207, 155, 107, 126, 152, 191, 70, 92, 222, 33, 186, 126, 36, 237, 255, 241, 83, 59, 158, 36, 254, 33, 121, 154, 223, 159, 17, 224, 161, 32, 102, 196, 62, 72, 21, 232, 35, 62, 171, 23, 7, 196, 32, 243, 47, 182, 226, 17, 82, 237, 81, 24, 85, 172, 55, 204, 195, 187, 195, 2, 49, 65, 68, 36, 100, 237, 179, 3, 182, 98, 3, 187, 167, 73, 93, 100, 215, 252, 128, 118, 204, 67, 224, 87, 176, 240, 248, 46, 25, 221, 76, 87, 233, 11, 37, 16, 142, 32, 230, 94, 145, 42, 73, 29, 216, 130, 206, 213, 110, 122, 141, 246, 163, 124, 147, 108, 214, 213, 68, 148, 116, 62, 5, 140 }, null, 2, "egIlLM1ZdUOlX7Yn/wYi5g==", new DateTime(2022, 10, 16, 15, 42, 55, 458, DateTimeKind.Local).AddTicks(2709) }
                });

            migrationBuilder.InsertData(
                table: "Receipts",
                columns: new[] { "Id", "CustomerId", "IsCheckedOut", "OperationDate" },
                values: new object[,]
                {
                    { 1, 1, true, new DateTime(2021, 7, 5, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 1, true, new DateTime(2021, 8, 10, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "Receipts",
                columns: new[] { "Id", "CustomerId", "OperationDate" },
                values: new object[] { 3, 2, new DateTime(2021, 10, 15, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CustomerId", "Logon", "Password", "PasswordResetToken", "PasswordSalt", "ResetTokenExpires", "UserRoleId", "VerificationToken", "VerifiedAt" },
                values: new object[,]
                {
                    { 3, 1, "smith@gmail.com", new byte[] { 60, 127, 254, 35, 254, 155, 93, 215, 39, 233, 238, 45, 230, 122, 167, 140, 75, 167, 252, 39, 20, 179, 222, 150, 192, 218, 56, 171, 133, 87, 103, 236, 84, 164, 47, 89, 200, 158, 74, 17, 247, 38, 248, 214, 22, 174, 251, 227, 197, 98, 78, 178, 228, 133, 110, 27, 116, 150, 21, 100, 114, 163, 178, 76 }, null, new byte[] { 210, 152, 172, 8, 86, 88, 79, 91, 235, 219, 177, 203, 194, 19, 228, 35, 249, 105, 158, 73, 20, 26, 121, 62, 22, 161, 72, 117, 60, 77, 214, 216, 69, 27, 73, 67, 218, 167, 221, 182, 155, 191, 184, 222, 214, 98, 91, 52, 86, 255, 171, 17, 6, 93, 234, 96, 168, 96, 183, 216, 206, 7, 164, 209, 62, 153, 235, 14, 2, 30, 69, 230, 109, 225, 115, 38, 53, 220, 190, 187, 133, 68, 248, 84, 248, 123, 23, 106, 63, 156, 141, 37, 15, 4, 64, 7, 26, 29, 180, 142, 46, 61, 204, 6, 211, 247, 99, 118, 92, 111, 118, 75, 54, 70, 175, 46, 51, 37, 233, 251, 169, 123, 183, 183, 196, 58, 102, 138 }, null, 3, "Osw5ppHij0SJPr5VJn5GYg==", new DateTime(2022, 10, 16, 15, 42, 55, 458, DateTimeKind.Local).AddTicks(3586) },
                    { 4, 2, "will@aol.com", new byte[] { 55, 108, 201, 39, 65, 12, 228, 101, 196, 192, 119, 23, 182, 2, 175, 137, 174, 235, 152, 154, 96, 46, 69, 33, 47, 119, 144, 165, 130, 82, 116, 19, 3, 83, 117, 207, 3, 7, 151, 197, 244, 110, 166, 109, 69, 22, 197, 109, 108, 89, 182, 39, 22, 225, 116, 125, 234, 233, 138, 188, 26, 136, 97, 122 }, null, new byte[] { 218, 197, 97, 218, 251, 57, 142, 42, 148, 2, 32, 151, 45, 192, 155, 209, 14, 170, 29, 133, 150, 152, 56, 175, 168, 246, 68, 22, 164, 194, 254, 126, 160, 184, 193, 14, 193, 253, 93, 181, 157, 227, 59, 78, 55, 123, 218, 255, 83, 106, 97, 203, 181, 93, 232, 143, 9, 79, 220, 159, 237, 168, 241, 116, 68, 181, 57, 26, 105, 151, 70, 228, 200, 144, 232, 255, 89, 77, 104, 22, 223, 246, 209, 239, 183, 142, 99, 108, 138, 176, 57, 21, 217, 211, 175, 4, 212, 115, 163, 226, 233, 230, 16, 162, 199, 156, 252, 23, 7, 93, 108, 87, 35, 81, 116, 41, 201, 149, 61, 217, 67, 55, 180, 76, 99, 211, 145, 222 }, null, 3, "HrVK/AEFeEOyUbX553UVVw==", new DateTime(2022, 10, 16, 15, 42, 55, 458, DateTimeKind.Local).AddTicks(3655) }
                });

            migrationBuilder.InsertData(
                table: "ReceiptsDetails",
                columns: new[] { "ReceiptId", "ProductId", "DiscountUnitPrice", "Id", "Quantity", "UnitPrice" },
                values: new object[,]
                {
                    { 1, 1, 16m, 1, 3, 20m },
                    { 1, 2, 40m, 2, 1, 50m },
                    { 2, 2, 40m, 3, 2, 50m },
                    { 3, 1, 18m, 4, 2, 20m },
                    { 3, 2, 45m, 5, 5, 50m }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Customers_PersonId",
                table: "Customers",
                column: "PersonId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_ProductCategoryId",
                table: "Products",
                column: "ProductCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Receipts_CustomerId",
                table: "Receipts",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceiptsDetails_ProductId",
                table: "ReceiptsDetails",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CustomerId",
                table: "Users",
                column: "CustomerId",
                unique: true,
                filter: "[CustomerId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserRoleId",
                table: "Users",
                column: "UserRoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ReceiptsDetails");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Receipts");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "ProductCategories");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Persons");
        }
    }
}
