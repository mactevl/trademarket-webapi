﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ReceiptRepository : Repository<Receipt>, IReceiptRepository
    {
        public ReceiptRepository(TradeMarketDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<IEnumerable<Receipt>> GetAllWithDetailsAsync()
        {
            return await _dbSet
                            .Include(c => c.Customer)
                                .ThenInclude(c => c.Person)
                            .Include(c => c.ReceiptDetails)  
                                .ThenInclude(rd => rd.Product)
                                    .ThenInclude(p => p.Category)                            
                            .ToListAsync();
        }

        public override async Task<Receipt> GetByIdWithDetailsAsync(int id)
        {
            return await _dbSet.Where(c => c.Id == id)
                            .Include(c => c.Customer)
                            .Include(c => c.ReceiptDetails)
                                .ThenInclude(rd => rd.Product)
                                    .ThenInclude(p => p.Category)
                            .FirstOrDefaultAsync();
        }

    }
}
