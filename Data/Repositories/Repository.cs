﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly TradeMarketDbContext _DbContext;
        protected readonly DbSet<TEntity> _dbSet;
        
        protected Repository(TradeMarketDbContext dbContext)
        {
            _DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dbSet = _DbContext.Set<TEntity>(); 
        }

        #region DefaultMethods

        public virtual async Task AddAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            if (entity != null)
                _dbSet.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var toBeDeleted = await _dbSet.SingleOrDefaultAsync(p => p.Id == id);
            _dbSet.Remove(toBeDeleted);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _dbSet
                .ToListAsync();
        }

        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual void Update(TEntity entity)
        {
            var entityToUpdate = _dbSet.Find(entity.Id);
            _DbContext.Entry(entityToUpdate).CurrentValues.SetValues(entity);
           
        }

        #endregion DefaultMethod

        #region AbstractMethods

        public abstract Task<IEnumerable<TEntity>> GetAllWithDetailsAsync();

        public abstract Task<TEntity> GetByIdWithDetailsAsync(int id);

        #endregion AbstractMethods
    }
}
