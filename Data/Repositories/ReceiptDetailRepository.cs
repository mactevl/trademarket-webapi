﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ReceiptDetailRepository : Repository<ReceiptDetail>, IReceiptDetailRepository
    {
        public ReceiptDetailRepository(TradeMarketDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<ReceiptDetail> GetByIdAsync(int id)
        {
            var result = await _dbSet.SingleOrDefaultAsync(rd => rd.Id == id);

            return result;
        }

        public override async void Update(ReceiptDetail entity)
        {
            var entityToUpdate = await _dbSet.SingleOrDefaultAsync(rd => rd.Id == entity.Id);
            _DbContext.Entry(entityToUpdate).CurrentValues.SetValues(entity);

        }

        public override async Task<IEnumerable<ReceiptDetail>> GetAllWithDetailsAsync()
        {
            return await _dbSet
                        .Include(rd => rd.Receipt)
                        .Include(rd => rd.Product)
                        .Include(rd => rd.Product.Category)
                        .ToListAsync();
        }

        public override Task<ReceiptDetail> GetByIdWithDetailsAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
