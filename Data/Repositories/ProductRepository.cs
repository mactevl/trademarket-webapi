﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(TradeMarketDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<IEnumerable<Product>> GetAllWithDetailsAsync()
        {
            return await _dbSet
                .Include(rd => rd.Category)
                .Include(rd => rd.ReceiptDetails)
                .ToListAsync();
        }

       
        public override async Task<Product> GetByIdWithDetailsAsync(int id)
        {
            return await _dbSet
                .Where(c => c.Id == id)
                .Include(c => c.Category)
                .Include(c => c.ReceiptDetails)
                .FirstOrDefaultAsync();
        }

    }
}
