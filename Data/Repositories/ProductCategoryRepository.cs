﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class ProductCategoryRepository : Repository<ProductCategory>, IProductCategoryRepository
    {
        public ProductCategoryRepository(TradeMarketDbContext dbContext) : base(dbContext)
        {
        }

        public override Task<IEnumerable<ProductCategory>> GetAllWithDetailsAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<ProductCategory> GetByIdWithDetailsAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
