﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class PersonRepository : Repository<Person>, IPersonRepository
    {
        public PersonRepository(TradeMarketDbContext dbContext) : base(dbContext)
        {
        }

        public override Task<IEnumerable<Person>> GetAllWithDetailsAsync()
        {
            throw new NotImplementedException();
        }

        public override Task<Person> GetByIdWithDetailsAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
