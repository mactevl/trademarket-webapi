﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using static Microsoft.EntityFrameworkCore.Design.OperationExecutor;

namespace Data.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(TradeMarketDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<IEnumerable<Customer>> GetAllWithDetailsAsync()
        {
            return await _dbSet
                .Include(c => c.Person)
                .Include(c => c.Receipts)
                    .ThenInclude(r => r.ReceiptDetails)
                .ToListAsync();
            //.Select(c => new Customer
            //{
            //    Id = c.Id,
            //    PersonId = c.PersonId,
            //    DiscountValue = c.DiscountValue,
            //    Person = new Person()
            //    {
            //        Id = c.PersonId,
            //        Name = c.Person.Name,
            //        Surname = c.Person.Surname,
            //        BirthDate = c.Person.BirthDate,
            //    },
            //    Receipts = c.Receipts.Select(r => new Receipt()
            //    {
            //        Id = r.Id,
            //        CustomerId = r.CustomerId,
            //        OperationDate = r.OperationDate,
            //        IsCheckedOut = r.IsCheckedOut,
            //        ReceiptDetails = r.ReceiptDetails.Select(rd => new ReceiptDetail()
            //        {
            //            Id = rd.Id,
            //            ReceiptId = rd.ReceiptId,
            //            ProductId = rd.ProductId,
            //            DiscountUnitPrice = rd.DiscountUnitPrice,
            //            UnitPrice = rd.UnitPrice,
            //            Quantity = rd.Quantity
            //        }).ToList()
            //    }).ToList()
            //})
        }

        public override async Task<Customer> GetByIdWithDetailsAsync(int id)
        {
            var allCustomer = await _dbSet
                .Include(c => c.Person)
                .Include(c => c.Receipts)
                    .ThenInclude(r => r.ReceiptDetails)
                .ToListAsync();
            var customerToReturn = allCustomer.SingleOrDefault(c => c.Id == id);

            return customerToReturn;
        }

        public async override void Update(Customer entity)
        {
            var customer = await GetByIdWithDetailsAsync(entity.Id);
            if(customer?.Person != null && entity?.Person != null)
                _DbContext.Entry(customer.Person).CurrentValues.SetValues(entity.Person);
            _DbContext.Entry(customer).CurrentValues.SetValues(entity);
            
            

        }

        public override void Delete(Customer entity)
        {
            if (entity != null)
            {
                _dbSet.Remove(entity);
                _DbContext.Persons.Remove(entity.Person);
            }
                
        }
    }
}
