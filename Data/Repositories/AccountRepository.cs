﻿using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class AccountRepository: Repository<User>, IAccountRepository
    {
        public AccountRepository(TradeMarketDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<User> GetUserByLoginAsync(string username)
        {
            var allUsers = await _dbSet
                .Include(u => u.Role)
                .ToListAsync();

            var result = allUsers.SingleOrDefault(u => u.Logon.Equals(username));
            return result;
        }

        public async Task<User> GetUserWithDetailsByLoginAsync(string username)
        {
            var allUsers = await _dbSet
                .Include(u => u.Role)
                .Include(u => u.Customer)
                .Include(u => u.Customer.Person)
                .Include(u => u.Customer.Receipts)
                .ThenInclude(r => r.ReceiptDetails)
                .ToListAsync();

            var result = allUsers.SingleOrDefault(u => u.Logon.Equals(username));
            return result;
        }


        public async Task<User> GetUserByTokenAsync(string Token)
        {
            var allUsers = await _dbSet
                .ToListAsync();

            var result = allUsers.SingleOrDefault(u => u.VerificationToken.Equals(Token) || 
                                                       (u.PasswordResetToken ?? string.Empty) == Token &&
                                                       u.VerificationToken != (u.PasswordResetToken ?? string.Empty));
            return result;
        }

        public async override Task<IEnumerable<User>> GetAllWithDetailsAsync()
        {
            //no details
            var allUsers = await _dbSet.ToListAsync();
            return allUsers;
        }

        public override Task<User> GetByIdWithDetailsAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
