﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Data.Configurations
{
    public class ProductConfguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.Property(prop => prop.ProductCategoryId).IsRequired();
            builder.Property(prop => prop.ProductName)
                .HasMaxLength(40)
                .IsUnicode()
                .IsRequired();
            builder.Property(prop => prop.Price)
                .HasColumnType("decimal(7,2)")
                .IsRequired();

            builder.HasOne(r => r.Category)
                .WithMany(c => c.Products)
                .HasForeignKey(p => p.ProductCategoryId);

            builder.HasData(
                new Product { Id = 1, ProductCategoryId = 1, ProductName = "Name1", Price = 20 },
                new Product { Id = 2, ProductCategoryId = 2, ProductName = "Name2", Price = 50 }
                );
        }
    }
}
