﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Data.Configurations
{
    public class CustomerConfguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(prop => prop.PersonId).IsRequired();
            builder.Property(prop => prop.DiscountValue).HasDefaultValue(0);

            builder.HasOne(c => c.Person).WithOne(p => p.Customer).HasForeignKey<Customer>(c => c.PersonId);

            builder.HasData(
                new Customer { Id = 1, PersonId = 1, DiscountValue = 20 },
                new Customer { Id = 2, PersonId = 2, DiscountValue = 10 }
                );
        }
    }
}
