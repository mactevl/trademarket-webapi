﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Data.Configurations
{
    public class UserRoleConfguration : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.Property(prop => prop.RoleName)
                .IsUnicode()
                .HasMaxLength(50)
                .IsRequired();

            builder.HasData(
                new UserRole 
                { 
                    Id = 1,
                    RoleName = "Administrator"
                },
                new UserRole
                {
                    Id = 2,
                    RoleName = "Manager"
                },
                new UserRole 
                {
                    Id = 3,
                    RoleName = "Client"
                }
                );
        }
    }
}
