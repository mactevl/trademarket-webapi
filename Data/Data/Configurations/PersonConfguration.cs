﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Data.Configurations
{
    public class PersonConfguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.Property(prop => prop.Name)
                .HasMaxLength(30)
                .IsUnicode()
                .IsRequired();

            builder.Property(prop => prop.Surname).HasMaxLength(50)
                .IsUnicode()
                .IsRequired();

            builder.HasData(
                new Person { Id = 1, Name = "Name1", Surname = "Surname1", BirthDate = new DateTime(1980, 5, 25) },
                new Person { Id = 2, Name = "Name2", Surname = "Surname2", BirthDate = new DateTime(1984, 10, 19) }
                );

        }
    }
}
