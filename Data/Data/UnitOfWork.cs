﻿using Data.Entities;
using Data.Interfaces;
using Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Data.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TradeMarketDbContext _DbContext;
        private readonly ICustomerRepository _customerRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductCategoryRepository _productCategoryRepository;
        private readonly IReceiptRepository _receiptRepository;
        private readonly IReceiptDetailRepository _receiptDetailRepository;

        public ICustomerRepository CustomerRepository => _customerRepository;
        public IAccountRepository AccountRepository => _accountRepository;
        public IPersonRepository PersonRepository => _personRepository;
        public IProductRepository ProductRepository => _productRepository;
        public IProductCategoryRepository ProductCategoryRepository => _productCategoryRepository;
        public IReceiptRepository ReceiptRepository => _receiptRepository;
        public IReceiptDetailRepository ReceiptDetailRepository => _receiptDetailRepository;

        public UnitOfWork(TradeMarketDbContext DbContext)
        {
            _DbContext = DbContext;
            _customerRepository = new CustomerRepository(this._DbContext);
            _accountRepository = new AccountRepository(this._DbContext);
            _personRepository = new PersonRepository(this._DbContext);
            _productRepository = new ProductRepository(this._DbContext);
            _productCategoryRepository = new ProductCategoryRepository(this._DbContext);
            _receiptRepository = new ReceiptRepository(this._DbContext);
            _receiptDetailRepository = new ReceiptDetailRepository(this._DbContext);
        }

        public IRepository<TEntity> GetRepository<TEntity>(TEntity entity) where TEntity : class
        {
            var unitOfWorkType = typeof(UnitOfWork);
            var propertyInfos = unitOfWorkType.GetProperties();
            var propertyInfo = propertyInfos.FirstOrDefault(pi => pi.Name.Contains(typeof(TEntity).Name));
            var repo = (IRepository<TEntity>)propertyInfo.GetValue(this);
            return repo;
        }

        public async Task SaveAsync()
        {
            await _DbContext.SaveChangesAsync();
        }
    }
}
