﻿using Data.Data.Configurations;
using Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Data
{
    public class TradeMarketDbContext : DbContext
    {

        //public TradeMarketDbContext()
        //{
        //}

        public TradeMarketDbContext(DbContextOptions<TradeMarketDbContext> options) : base(options)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=TradeMarket;Trusted_Connection=True;");
        //}

        public DbSet<User> Users { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Receipt> Receipts { get; set; }
        public DbSet<ReceiptDetail> ReceiptsDetails { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfguration());
            modelBuilder.ApplyConfiguration(new CustomerConfguration());
            modelBuilder.ApplyConfiguration(new PersonConfguration());
            modelBuilder.ApplyConfiguration(new ReceiptConfguration());
            modelBuilder.ApplyConfiguration(new ReceiptDetailConfguration());
            modelBuilder.ApplyConfiguration(new ProductConfguration());
            modelBuilder.ApplyConfiguration(new ProductCategoryConfguration());
        }
        
    }
}
