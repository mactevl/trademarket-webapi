﻿using Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        new Task<IEnumerable<Product>> GetAllWithDetailsAsync();

        new Task<Product> GetByIdWithDetailsAsync(int id);
    }
}
