﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IAccountRepository : IRepository<User>
    {
        Task<User> GetUserByLoginAsync(string username);
        Task<User> GetUserWithDetailsByLoginAsync(string username);
        Task<User> GetUserByTokenAsync(string Token);


    }
}
