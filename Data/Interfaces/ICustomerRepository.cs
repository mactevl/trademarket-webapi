﻿using Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        new Task<IEnumerable<Customer>> GetAllWithDetailsAsync();

        new Task<Customer> GetByIdWithDetailsAsync(int id);
    }
}
