﻿using Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Interfaces
{
    public interface IReceiptRepository : IRepository<Receipt>
    {
        new Task<IEnumerable<Receipt>> GetAllWithDetailsAsync();

        new Task<Receipt> GetByIdWithDetailsAsync(int id);
    }
}
