﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    public class CustomerRegistrationModel
    {
        //CustomerModel object fields
        public int? CustomerId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        [Range(typeof(DateTime), "1/1/1920", "1/1/2004",
            ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public DateTime BirthDate { get; set; }

        //User object field
        [Required, EmailAddress]
        public string Logon { get; set; }

        [Required, MinLength(8)]
        public string Password { get; set; }
        
        [Required, Compare("Password")]
        public string ConfirmPassword { get; set; }


    }
}
