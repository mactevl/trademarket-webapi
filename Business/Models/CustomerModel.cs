﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    public class CustomerModel : BaseModel
    {
        [Required(ErrorMessage = "Customer's name can not be null!")]
        [MaxLength(30, ErrorMessage = "Customer's name should be less than 30 characters!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Customer's surname can not be null!")]
        [MaxLength(50, ErrorMessage = "Customer's surname should be less than 50 characters!")]
        public string Surname { get; set; }

        [Range(typeof(DateTime), "1/1/1920", "1/1/2004",
            ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public DateTime BirthDate { get; set; }

        [Range(0, 100)]
        public int DiscountValue { get; set; }

        public ICollection<int> ReceiptsIds { get; set; }
    }
}
