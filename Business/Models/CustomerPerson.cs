﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class CustomerPerson
    {
        public Person Person { get; set; } 
        public Customer Customer { get; set; } 
    }
}
