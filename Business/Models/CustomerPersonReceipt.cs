﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Models
{
    public class CustomerPersonReceipt : CustomerPerson
    {
        public Receipt Receipt { get; set; } 
    }
}
