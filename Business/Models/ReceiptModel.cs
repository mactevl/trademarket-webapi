﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    public class ReceiptModel : BaseModel
    {
        [Required]
        public int CustomerId { get; set; }

        [Required]
        public DateTime OperationDate { get; set; }
        public bool IsCheckedOut { get; set; }

        public ICollection<int> ReceiptDetailsIds { get; set; }
    }
}
