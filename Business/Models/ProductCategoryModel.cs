﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    public class ProductCategoryModel : BaseModel
    {
        [Required]
        [MinLength(1), MaxLength(150)]
        public string CategoryName { get; set; }

        public ICollection<int> ProductIds { get; set; }
    }
}
