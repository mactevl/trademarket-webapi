﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    public class ProductModel : BaseModel
    {
        [Required]
        public int ProductCategoryId { get; set; }

        [Required]
        public string ProductName { get; set; }

        public string CategoryName { get; set; }

        [Required]
        public decimal Price { get; set; }

        public ICollection<int> ReceiptDetailIds { get; set; }
    }
}
