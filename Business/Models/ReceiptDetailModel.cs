﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Business.Models
{
    public class ReceiptDetailModel : BaseModel
    {
        [Required]
        public int ReceiptId { get; set; }

        [Required]
        public int ProductId { get; set; }

        public decimal DiscountUnitPrice { get; set; }

        [Required]
        public decimal UnitPrice { get; set; }

        [Required]
        [Range(0,999)]
        public int Quantity { get; set; }
    }
}
