﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    public class UserLoginModel : BaseModel
    {
        [Required, EmailAddress]
        public string Logon { get; set; }

        [Required, MinLength(8)]
        public string Password { get; set; }
    }
}
