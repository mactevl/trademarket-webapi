﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Business.Models
{
    public class ResetPasswordRequest
    {
        [Required]
        public string Token { get; set; }

        [Required, MinLength(8)]
        public string NewPassword { get; set; }
        
        [Required, Compare("NewPassword")]
        public string ConfirmNewPassword { get; set; }


    }
}
