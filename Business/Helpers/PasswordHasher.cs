﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Business.Helpers
{
    /// <summary>
    /// A static class that represents the service for getting hashed user's passwords. 
    /// </summary>
    public static class PasswordHasher
    {
        /// <summary>
        /// A static method which hashes strings.
        /// </summary>
        /// <param name="password">A password, provided by the user through a registration process.</param>
        /// <returns>User's password in a form of hashed array of bytes.</returns>
        public static byte[] GetHash(string password)
        {
            var passwordBytes = Encoding.Unicode.GetBytes(password);
            var passwordHash = SHA512.Create().ComputeHash(passwordBytes);
            return passwordHash;
        }

        public static void GetSaltedHash(string password,
                                     out byte[] passwordHash,
                                     out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.Unicode.GetBytes(password));
            }
        }

        public static bool VerifySaltedPassword(string password,
                                    byte[] passwordHash,
                                    byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.Unicode.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }
        }

    }
}
