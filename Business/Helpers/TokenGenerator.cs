﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Business.Helpers
{
    /// <summary>
    /// A static class that represents the service for getting custom tokens. 
    /// </summary>
    public static class TokenGenerator
    {
        /// <summary>
        /// A static method which creates a custom token.
        /// </summary>
        /// <returns>A custom token in a form of an array of bytes.</returns>
        public static string GetRandomToken()
        {
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray());
        }
    }
}
