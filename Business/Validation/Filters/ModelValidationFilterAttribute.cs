﻿using Business.Models;
using Business.Validation.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Validation.Filters
{
    public class ModelValidationFilterAttribute : Attribute, IAsyncActionFilter 
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            BaseModel model = context.ActionArguments.Values.SingleOrDefault(v => v is BaseModel) as BaseModel;

            if(model is null)
            {
                context.Result = new BadRequestObjectResult("An object expected in the body, but was null!");
                return;
            }

            try
            {
                model.IsValidModel();
            }
            catch(MarketException)
            {
                context.Result = new BadRequestObjectResult("Invalid object received!");
                return;
            }

            await next();
        }
    }
}
