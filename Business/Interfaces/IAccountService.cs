﻿using Business.Models;
using Data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business.Interfaces
{
    public interface IAccountService : ICrud<UserLoginModel>
    {
        Task<IEnumerable<User>> GetAllUsersAsync();
        Task<User> GetUserByLoginAsync(string username);
        Task<User> GetUserWithDetailsByLoginAsync(string username);
        Task<User> GetUserByTokenAsync(string token);
        Task<User> GetUserWithDetailsByTokenAsync(string token);
        Task RegisterNewUser(CustomerRegistrationModel signUpModel);
        CustomerModel GetCustomerModel(CustomerRegistrationModel signUpModel);
        Task GetPasswordResetTokenEmailAsync(string email);
        Task<bool> ResetPasswordAsync(ResetPasswordRequest resetPasswordRequest);
        Task<bool> IsVarified(string token);
        Task<bool> IsInUsers(string parameter);


    }
}