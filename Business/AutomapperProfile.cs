﻿using AutoMapper;
using Business.Models;
using Data.Entities;
using System.Linq;

namespace Business
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Receipt, ReceiptModel>()
                .ForMember(rm => rm.ReceiptDetailsIds, r => r.MapFrom(x => x.ReceiptDetails.Select(rd => rd.Id)))
                .ReverseMap();

            CreateMap<Product, ProductModel>()
                .ForMember(pm => pm.CategoryName, cn => cn.MapFrom(p => p.Category.CategoryName))
                .ForMember(pm => pm.ReceiptDetailIds, rdi => rdi.MapFrom(p => p.ReceiptDetails.Select(rd => rd.Id)))
                .ReverseMap();

            CreateMap<ReceiptDetail, ReceiptDetailModel>()                
                .ReverseMap();

            CreateMap<Customer, Customer>().ReverseMap();
            CreateMap<Person, Person>().ReverseMap();

            CreateMap<Customer, CustomerPerson>()
               .ForMember(cp => cp.Customer, id => id.MapFrom(c => c))
               .ForMember(cp => cp.Person, n => n.MapFrom(c => c.Person))
               .ReverseMap();

            //Create mapping that combines Customer and Person into CustomerModel
            CreateMap<CustomerPerson, CustomerModel>()
                .ForMember(cm => cm.Id, id => id.MapFrom(cp => cp.Customer.Id))
                .ForMember(cm => cm.Id, id => id.MapFrom(cp => cp.Person.Id))
                .ForMember(cm => cm.Name, n => n.MapFrom(cp => cp.Person.Name))
                .ForMember(cm => cm.Surname, s => s.MapFrom(cp => cp.Person.Surname))
                .ForMember(cm => cm.BirthDate, bd => bd.MapFrom(cp => cp.Person.BirthDate))
                .ForMember(cm => cm.DiscountValue, dv => dv.MapFrom(cp => cp.Customer.DiscountValue))
                .ForMember(cm => cm.ReceiptsIds, ri => ri.MapFrom(cp => cp.Customer.Receipts.Select(r => r.Id)))
                .ReverseMap();

            //Create mapping for ProductCategory and ProductCategoryModel
            CreateMap<ProductCategory, ProductCategoryModel>()
                .ForMember(pcm => pcm.ProductIds, pi => pi.MapFrom(pc => pc.Products.Select(p => p.Id)))
                .ReverseMap();

            //Map for Customer, Person, Receipt and CustomerActivityModel
            CreateMap<CustomerPersonReceipt, CustomerActivityModel>()
                .ForMember(cam => cam.CustomerId, ci => ci.MapFrom(cpr => cpr.Customer.Id))
                .ForMember(cam => cam.CustomerName, p => p.MapFrom(cpr => cpr.Person.Name + " " + cpr.Person.Surname))
                .ForMember(cam => cam.ReceiptSum, r => r.MapFrom(cpr => cpr.Receipt.ReceiptDetails.Select(rd => rd.UnitPrice - (rd.DiscountUnitPrice * rd.UnitPrice) * rd.Quantity).Sum()))
                .ReverseMap();

            //Mapping for CustomerRegistrationModel <=> CustomerModel
            CreateMap<CustomerRegistrationModel, CustomerModel>();

        }
    }
}