﻿
using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Business.Validation;
using Data.Entities;
using Business.Validation.Extensions;
using Data.Repositories;
using System.Net.Mail;
using System.Net;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Business.Services
{
    public class CustomerService : Crud<CustomerModel, Customer>, ICustomerService
    {
        public CustomerService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, unitOfWork.CustomerRepository, mapper)
        {
        }

        public override async Task<IEnumerable<CustomerModel>> GetAllAsync()
        {
            var entities = await _unitOfWork.CustomerRepository.GetAllWithDetailsAsync();

            var customerPerson = _mapper.Map<IEnumerable<CustomerPerson>>(entities);
            
            return _mapper.Map<IEnumerable<CustomerModel>>(customerPerson);
        }

        public override async Task<CustomerModel> GetByIdAsync(int id)
        {
            var entity = await _unitOfWork.CustomerRepository.GetByIdWithDetailsAsync(id);

            if (entity == null)
                return null;

            CustomerPerson newCustomerPerson = new CustomerPerson { Customer = entity, Person = entity.Person };

            var customerModel = _mapper.Map<CustomerModel>(newCustomerPerson);

            return customerModel;

        }

        public override async Task AddAsync(CustomerModel customerModel)
        {
            if (customerModel.IsValidModel())
            {
                var customerPersonEntity = _mapper.Map<CustomerPerson>(customerModel);

                Customer newCustomer = customerPersonEntity.Customer;
                newCustomer.Person = customerPersonEntity.Person;

                await _unitOfWork.CustomerRepository.AddAsync(newCustomer);

                await _unitOfWork.SaveAsync();
            }
        }

        public override async Task UpdateAsync(CustomerModel customerModel)
        {
            if (customerModel.IsValidModel())
            {               
                var customerPersonToUpdate = _mapper.Map<CustomerPerson>(customerModel);
               
                customerPersonToUpdate.Person.Id = (int)customerModel.Id;
                customerPersonToUpdate.Customer.PersonId = (int)customerModel.Id;
                customerPersonToUpdate.Customer.Person = customerPersonToUpdate.Person;
                customerPersonToUpdate.Person.Customer = customerPersonToUpdate.Customer;

                //_mapper.Map(customerPersonToUpdate.Customer, customer);
                _repository.Update(customerPersonToUpdate.Customer);

                await _unitOfWork.SaveAsync();
            }

        }

        public async Task<IEnumerable<CustomerModel>> GetCustomersByProductIdAsync(int productId)
        {
            //var isInProducts = await _unitOfWork.ProductRepository.GetAllAsync();
            //if (!isInProducts.Any(p => p.Id == productId))
            //    throw new MarketException("There is no such a product in the system!");

            var allcustomers = await _unitOfWork.CustomerRepository.GetAllWithDetailsAsync();
            var filteredCustomerPersons = allcustomers.Where(c => c.Receipts.Any(r => r.ReceiptDetails.Any(rd => rd.ProductId == productId))).Select(c => new CustomerPerson { Customer = c, Person = c.Person });

            return _mapper.Map<IEnumerable<CustomerModel>>(filteredCustomerPersons);
        }
            
    }
}
