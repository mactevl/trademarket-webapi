﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Business.Validation.Extensions;
using Data.Entities;
using Data.Interfaces;
using Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class ReceiptService : Crud<ReceiptModel, Receipt>, IReceiptService
    {
        public ReceiptService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, unitOfWork.ReceiptRepository , mapper)
        {
        }

        public override async Task<IEnumerable<ReceiptModel>> GetAllAsync()
        {
            var entities = await _unitOfWork.ReceiptRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<ReceiptModel>>(entities);
        }

        public override async Task<ReceiptModel> GetByIdAsync(int id)
        {
            var entity = await _unitOfWork.ReceiptRepository.GetByIdWithDetailsAsync(id);
            return _mapper.Map<ReceiptModel>(entity);

        }

        public override async Task DeleteAsync(int modelId)
        {
            var toBeDeleted = await _unitOfWork.ReceiptRepository.GetByIdWithDetailsAsync(modelId);
            var receiptDetails = toBeDeleted.ReceiptDetails.ToList();

            foreach (var item in receiptDetails)
            {
                _unitOfWork.ReceiptDetailRepository.Delete(item);
            }

            await _unitOfWork.ReceiptRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();

        }

        public async Task AddProductAsync(int productId, int receiptId, int quantity)
        {

            var receiptToUpdate = await _unitOfWork.ReceiptRepository.GetByIdWithDetailsAsync(receiptId);

            if (receiptToUpdate == null)
                throw new MarketException("The receipt was not founed!");

            if (receiptToUpdate.ReceiptDetails != null && 
                receiptToUpdate.ReceiptDetails.Any(rd => rd.ProductId == productId))
            {
                receiptToUpdate.ReceiptDetails.FirstOrDefault(rd => rd.ProductId == productId).Quantity += quantity;
            }
            else
            {
                var productToAdd = await _unitOfWork.ProductRepository.GetByIdAsync(productId);
                if (productToAdd == null)
                    throw new MarketException("There is no such a product registered!");

                int newReceiptDetailIndex = _unitOfWork.ReceiptDetailRepository.GetAllAsync().Result.Count() + 1;
                ReceiptDetail newProduct = new ReceiptDetail()
                {
                    Id = newReceiptDetailIndex,
                    ReceiptId = receiptId,
                    ProductId = productId,
                    Quantity = quantity,
                    DiscountUnitPrice = productToAdd.Price - (receiptToUpdate.Customer.DiscountValue / 100m * productToAdd.Price),
                    UnitPrice = productToAdd.Price,                    
                };

                await _unitOfWork.ReceiptDetailRepository.AddAsync(newProduct);
            }
            
            await _unitOfWork.SaveAsync();
            
        }

        public async Task RemoveProductAsync(int productId, int receiptId, int quantity)
        {
            var receiptToUpdate = await _unitOfWork.ReceiptRepository.GetByIdWithDetailsAsync(receiptId);

            if(receiptToUpdate.ReceiptDetails?.FirstOrDefault(rd => rd.ProductId == productId).Quantity - quantity == 0)
            {                
                _unitOfWork.ReceiptDetailRepository.Delete(receiptToUpdate.ReceiptDetails.FirstOrDefault(rd => rd.ProductId == productId));

                receiptToUpdate.ReceiptDetails.Remove(receiptToUpdate.ReceiptDetails.FirstOrDefault(rd => rd.ProductId == productId));
            }
            else
            {
                receiptToUpdate.ReceiptDetails.FirstOrDefault(rd => rd.ProductId == productId).Quantity -= quantity;
            }

            if (receiptToUpdate.ReceiptDetails.Count == 0)
            {
                _unitOfWork.ReceiptRepository.Delete(receiptToUpdate);
            }
            else
            {
                _unitOfWork.ReceiptRepository.Update(receiptToUpdate);
            }
            await _unitOfWork.SaveAsync();

        }

        public async Task<decimal> ToPayAsync(int receiptId)
        {
            var receiptToPay = await _unitOfWork.ReceiptRepository.GetByIdWithDetailsAsync(receiptId);
            if (receiptToPay.ReceiptDetails.Count > 0)
                return receiptToPay.ReceiptDetails.Select(rd => rd.DiscountUnitPrice * rd.Quantity).Sum();
            else
                return 0m;
        }

        public async Task CheckOutAsync(int receiptId)
        {
            var receiptToCheckOut = await _unitOfWork.ReceiptRepository.GetByIdAsync(receiptId);

            if (!receiptToCheckOut.IsCheckedOut)
                receiptToCheckOut.IsCheckedOut = true;

            _unitOfWork.ReceiptRepository.Update(receiptToCheckOut);

            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<ReceiptDetailModel>> GetReceiptDetailsAsync(int receiptId)
        {
            var receiptWithDetails = await _unitOfWork.ReceiptRepository.GetByIdWithDetailsAsync(receiptId);
            return _mapper.Map<IEnumerable<ReceiptDetailModel>>(receiptWithDetails.ReceiptDetails);
        }

        public async Task<IEnumerable<ReceiptModel>> GetReceiptsByPeriodAsync(DateTime startDate, DateTime endDate)
        {
            var allReceipts = await _unitOfWork.ReceiptRepository.GetAllWithDetailsAsync();
            var filteredReceipts = allReceipts.Where(x => x.OperationDate >= startDate && x.OperationDate <= endDate);
            return _mapper.Map<IEnumerable<ReceiptModel>>(filteredReceipts);
        }

    }
}
