﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Web;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Business.Helpers;
using System.Linq;

namespace Business.Services
{
    public class AccountService : Crud<UserLoginModel, User>, IAccountService
    {
        public AccountService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, unitOfWork.AccountRepository, mapper)
        {
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            var entities = await _repository.GetAllAsync();
            return entities;
        }

        public async Task<User> GetUserByLoginAsync(string username)
        {
            return await _unitOfWork.AccountRepository.GetUserByLoginAsync(username);
        }

        public async Task<User> GetUserWithDetailsByLoginAsync(string username)
        {
            return await _unitOfWork.AccountRepository.GetUserWithDetailsByLoginAsync(username);
        }

        public async Task<User> GetUserByTokenAsync(string token)
        {
            return await _unitOfWork.AccountRepository.GetUserByTokenAsync(token);
        }

        public async Task<User> GetUserWithDetailsByTokenAsync(string token)
        {
            User user = await GetUserByTokenAsync(token);
            return await GetUserWithDetailsByLoginAsync(user.Logon);
        }

        public async Task RegisterNewUser(CustomerRegistrationModel signUpModel)
        {
            byte[] passwordHash;
            byte[] passwordSalt;
            PasswordHasher.GetSaltedHash(signUpModel.Password, out passwordHash, out passwordSalt);

            User newUser = new User()
            {
                CustomerId = signUpModel.CustomerId,
                UserRoleId = 3,
                Logon = signUpModel.Logon,
                Password = passwordHash,
                PasswordSalt = passwordSalt,
                //HttpUtility.UrlEncode() convers a string to url valid encoding(converts special characters)
                VerificationToken = HttpUtility.UrlEncode(TokenGenerator.GetRandomToken())
            };

            await _repository.AddAsync(newUser);
            await _unitOfWork.SaveAsync();
            //Send email confirmation
        }

        public async Task<bool> IsVarified(string token)
        {
            var allUsers = await GetAllUsersAsync();
            User isInUsers = allUsers.SingleOrDefault(u => u.VerificationToken.Equals(HttpUtility.UrlEncode(token)));

            if (isInUsers == null)
                return false;

            if (isInUsers.VerifiedAt != null)
                return true;

            isInUsers.VerifiedAt = DateTime.Now;
            await _unitOfWork.SaveAsync();
            return true;
        }

        public async Task GetPasswordResetTokenEmailAsync(string email)
        {
            var user = await GetUserByLoginAsync(email);
            user.PasswordResetToken = TokenGenerator.GetRandomToken();
            user.ResetTokenExpires = DateTime.UtcNow.AddDays(1);
            await _unitOfWork.SaveAsync();
            //send an email
        }

        public async Task<bool> IsInUsers(string parameter)
        {
            var allUsers = await _repository.GetAllAsync();

            return allUsers.Any(u => u.Logon == parameter ||
                                     u.VerificationToken == parameter ||
                                     (u.PasswordResetToken ?? string.Empty) == parameter &&
                                     u.VerificationToken != (u.PasswordResetToken ?? string.Empty));
        }

        public async Task<bool> ResetPasswordAsync(ResetPasswordRequest resetPasswordRequest)
        {
            byte[] passwordHash;
            byte[] passwordSalt;
            PasswordHasher.GetSaltedHash(resetPasswordRequest.NewPassword, out passwordHash, out passwordSalt);
            User user = await GetUserByTokenAsync(resetPasswordRequest.Token);
            
            if(user.ResetTokenExpires < DateTime.UtcNow)            
                return false;
            
            user.Password = passwordHash;
            user.PasswordSalt = passwordSalt;
            user.PasswordResetToken = null;
            user.ResetTokenExpires = null;
            await _unitOfWork.SaveAsync();
            //Send email
            return true;
        }

        public CustomerModel GetCustomerModel(CustomerRegistrationModel signUpModel) => _mapper.Map<CustomerModel>(signUpModel);
    }
}
