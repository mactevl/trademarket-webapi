﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation.Extensions;
using Data.Data;
using Data.Entities;
using Data.Interfaces;
using Data.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Business.Services
{
    public class Crud<TModel, TEntity> : ICrud<TModel> where TModel : BaseModel where TEntity : class
    {
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly IRepository<TEntity> _repository;
        protected readonly IMapper _mapper;

        public Crud(IUnitOfWork unitOfWork, IRepository<TEntity> repo, IMapper mapper)
        {
            _repository = repo;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public virtual async Task AddAsync(TModel model)
        {
            if(model.IsValidModel())
            {
                var newEntity = _mapper.Map<TEntity>(model);

                await _repository.AddAsync(newEntity);

                await _unitOfWork.SaveAsync();
            }
            
        }

        public virtual async Task DeleteAsync(int modelId)
        {            
            await _repository.DeleteByIdAsync(modelId);

            await _unitOfWork.SaveAsync();

        }

        public virtual async Task<IEnumerable<TModel>> GetAllAsync()
        {
            var entities = await _repository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<TModel>>(entities);
        }

        public virtual async Task<TModel> GetByIdAsync(int id)
        {
            var entity = await _repository.GetByIdWithDetailsAsync(id);
            return _mapper.Map<TModel>(entity);

        }

        public virtual async Task UpdateAsync(TModel model)
        {
            if (model.IsValidModel())
            {
                var newEntity = _mapper.Map<TEntity>(model);

                _repository.Update(newEntity);

                await _unitOfWork.SaveAsync();
            }

        }

    }
}
