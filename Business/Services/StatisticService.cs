﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class StatisticService : IStatisticService
    {

        protected readonly IUnitOfWork _unitOfWork;
        protected readonly IMapper _mapper;

        public StatisticService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProductModel>> GetCustomersMostPopularProductsAsync(int productCount, int customerId)
        {
            var allReceipts = await _unitOfWork.ReceiptRepository.GetAllWithDetailsAsync();
            var allCustomersReceipts = allReceipts.Where(r => r.Customer.Id == customerId).ToList();

            var result = allCustomersReceipts.SelectMany(r => r.ReceiptDetails)
                .GroupBy(rd => rd.ProductId)
                .Select(g => new { Product = g.Select(rd => rd.Product).First(), 
                                   ProductSalesCount = g.Select(rd => rd.Quantity).Max() })
                .OrderByDescending(n => n.ProductSalesCount)
                .Select(n => n.Product)
                .Take(productCount)
                .ToList();

            return _mapper.Map<IEnumerable<ProductModel>>(result);
        }

        public async Task<decimal> GetIncomeOfCategoryInPeriod(int categoryId, DateTime startDate, DateTime endDate)
        {
            var allReceipts = await _unitOfWork.ReceiptRepository.GetAllWithDetailsAsync();

            var filteredByDate = allReceipts.Where(r => r.OperationDate >= startDate && r.OperationDate <= endDate).ToList();
            var filteredByCategory = filteredByDate.SelectMany(r => r.ReceiptDetails).Where(rd => rd.Product.ProductCategoryId == categoryId).ToList();
            return filteredByCategory.Select(rd => rd.DiscountUnitPrice * rd.Quantity).Sum();


        }

        public async Task<IEnumerable<ProductModel>> GetMostPopularProductsAsync(int productCount)
        {
            var allReceiptsDetails = await _unitOfWork.ReceiptDetailRepository.GetAllWithDetailsAsync();
            var mostPopularProducts = allReceiptsDetails
                .GroupBy(rd => rd.ProductId)
                .Select(g => new { 
                    Product = g.Select(rd => rd.Product).First(), 
                    ProductSalesCount = g.Select(rd => rd.Quantity)                    
                    .Sum() })
                .OrderByDescending(n => n.ProductSalesCount)
                .Select(n => n.Product)
                .Take(productCount);

            return _mapper.Map<IEnumerable<ProductModel>>(mostPopularProducts);
        }

        public async Task<IEnumerable<CustomerActivityModel>> GetMostValuableCustomersAsync(int customerCount, DateTime startDate, DateTime endDate)
        {
            var allReceipts = await _unitOfWork.ReceiptRepository.GetAllWithDetailsAsync();
            var filteredReceiptsByDate = allReceipts.Where(r => r.OperationDate >= startDate && r.OperationDate <= endDate).ToList();

            var customersOrderedByTotalSpend = filteredReceiptsByDate
                .GroupBy(r => r.CustomerId)
                .Select(g => new CustomerActivityModel 
                { CustomerId = g.Key, 
                  CustomerName = g.Select(r => r.Customer.Person.Name + " " + r.Customer.Person.Surname)
                    .First(), 
                  ReceiptSum = g.SelectMany(r => r.ReceiptDetails).Select( rd => rd.Quantity * rd.DiscountUnitPrice).Sum() 
                })
                .OrderByDescending(c => c.ReceiptSum)
                .Take(customerCount)
                .ToList();
            foreach (var item in customersOrderedByTotalSpend)
            {
                File.AppendAllText(@"C:\Users\macte\Documents\Debug.txt", "\n" + item.CustomerId.ToString() + ": " + item.CustomerName.ToString() + " - " + item.ReceiptSum.ToString() + "\n");
            }
            return customersOrderedByTotalSpend;
        }
    }
}
