﻿using AutoMapper;
using Business.Interfaces;
using Business.Models;
using Business.Validation;
using Business.Validation.Extensions;
using Data.Entities;
using Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Services
{
    public class ProductService : Crud<ProductModel, Product>, IProductService
    {
        public ProductService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, unitOfWork.ProductRepository, mapper)
        {
        }

        public override async Task<IEnumerable<ProductModel>> GetAllAsync()
        {
            var entities = await _unitOfWork.ProductRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<ProductModel>>(entities);
        }

        public override async Task<ProductModel> GetByIdAsync(int id)
        {
            var entity = await _unitOfWork.ProductRepository.GetByIdWithDetailsAsync(id);
            return _mapper.Map<ProductModel>(entity);

        }

        public async Task AddCategoryAsync(ProductCategoryModel categoryModel)
        {
            if(categoryModel.IsValidModel())
            {
                var categoryEntityToAdd = _mapper.Map<ProductCategory>(categoryModel);
                await _unitOfWork.ProductCategoryRepository.AddAsync(categoryEntityToAdd);
                await _unitOfWork.SaveAsync();
            }            
        }

        public async Task<IEnumerable<ProductCategoryModel>> GetAllProductCategoriesAsync()
        {
            return _mapper.Map<IEnumerable<ProductCategoryModel>>(await _unitOfWork.ProductCategoryRepository.GetAllAsync());
        }        

        public async Task RemoveCategoryAsync(int categoryId)
        {
            //if(!await IsInCategories(categoryId))
            //    throw new MarketException("There is no such a category in the system!");

            await _unitOfWork.ProductCategoryRepository.DeleteByIdAsync(categoryId);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateCategoryAsync(ProductCategoryModel categoryModel)
        {
            if (categoryModel.IsValidModel())
            {
                var ProductCategoryEntity = _mapper.Map<ProductCategory>(categoryModel);

                _unitOfWork.ProductCategoryRepository.Update(ProductCategoryEntity);

                await _unitOfWork.SaveAsync();
            }
           
        }

        public async Task<IEnumerable<ProductModel>> GetByFilterAsync(FilterSearchModel filterSearch)
        {
            Dictionary<int, ProductModel> resultDictionary = new Dictionary<int, ProductModel>();
            var allProducts = await _unitOfWork.ProductRepository.GetAllWithDetailsAsync();

            if (filterSearch.CategoryId != null) 
            {
                //if (!await IsInCategories((int)filterSearch.CategoryId))
                //    throw new MarketException("There is no such a category in the system!");

                var filteredProducts = _mapper.Map<IEnumerable<ProductModel>>(allProducts.Where(p => p.Category.Id == filterSearch.CategoryId));

                if(filteredProducts.Any())
                    CollectFilteredProducts(filteredProducts, resultDictionary);                
            }        

            if (filterSearch.MinPrice != null)
                FilterByMinPrice((int)filterSearch.MinPrice, allProducts, resultDictionary);            
                

            if (filterSearch.MaxPrice != null)
                FilterByMaxPrice((int)filterSearch.MaxPrice, allProducts, resultDictionary);


            return resultDictionary.Values;

        }

        public override async Task AddAsync(ProductModel productModel)
        {
            if (productModel.IsValidModel())
            {
                var productEntity = _mapper.Map<Product>(productModel);

                productEntity.Category = null;

                await _unitOfWork.ProductRepository.AddAsync(productEntity);

                await _unitOfWork.SaveAsync();
            }
        }

        #region ComplementaryMethods
        protected void FilterByMinPrice(int minPrice, IEnumerable<Product> allProducts, Dictionary<int, ProductModel> resultDictionary)
        {
            if (resultDictionary.Count == 0)
            {
                var filteredProducts = _mapper.Map<IEnumerable<ProductModel>>(allProducts.Where(p => p.Price >= minPrice));

                if (filteredProducts.Any())
                    CollectFilteredProducts(filteredProducts, resultDictionary);
            }
            else
            {
                var filteredProducts = resultDictionary.Values.Where(p => p.Price >= minPrice).ToList();
                if (filteredProducts.Any())
                {
                    resultDictionary.Clear();
                    CollectFilteredProducts(filteredProducts, resultDictionary);
                }
            }
        }
            

        protected void FilterByMaxPrice(int maxPrice, IEnumerable<Product> allProducts, Dictionary<int, ProductModel> resultDictionary)
        {
            if (resultDictionary.Count == 0)
            {
                var filteredProducts = _mapper.Map<IEnumerable<ProductModel>>(allProducts.Where(p => p.Price < maxPrice));

                if (filteredProducts.Any())
                    CollectFilteredProducts(filteredProducts, resultDictionary);
            }
            else
            {
                var filteredProducts = resultDictionary.Values.Where(p => p.Price < maxPrice).ToList();
                if (filteredProducts.Any())
                {
                    resultDictionary.Clear();
                    CollectFilteredProducts(filteredProducts, resultDictionary);
                }
            }
        }


        protected void CollectFilteredProducts(IEnumerable<ProductModel> source, Dictionary<int, ProductModel> destination)
        {
            foreach (var item in source)
            {
                if (!destination.ContainsKey(item.Id))
                    destination.Add(item.Id, item);
            }
        }

        protected async Task<bool> IsInCategories(int categoryId)
        {
            var isInCategories = await _unitOfWork.ProductCategoryRepository.GetAllAsync();
            return isInCategories.Any(c => c.Id == categoryId);
        }

        #endregion ComplementaryMethods
    }
}
